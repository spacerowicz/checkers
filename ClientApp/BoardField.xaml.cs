﻿using Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientApp
{
    /// <summary>
    /// Interaction logic for BoardField.xaml
    /// </summary>
    public partial class BoardField : UserControl
    {
        private Position position;
        private Brush enableSelectedDarkBrush = new SolidColorBrush(Color.FromRgb(103 + 40, 156 + 40, 43 + 40));
        private Brush enableSelectedLightBrush = new SolidColorBrush(Color.FromRgb(255, 243, 202));
        private Brush disableSelectedDarkBrush = new SolidColorBrush(Color.FromRgb(103 + 40, 156 + 40, 43 + 40));
        private Brush disableSelectedLightBrush = new SolidColorBrush(Color.FromRgb(255, 243, 202));
        private Brush enableDarkBrush = new SolidColorBrush(Color.FromRgb(73, 123, 28));
        private Brush enableLightBrush = new SolidColorBrush(Color.FromRgb(255, 236, 178));
        private Brush disableDarkBrush = new SolidColorBrush(Color.FromRgb(75, 75, 75));
        private Brush disableLightBrush = new SolidColorBrush(Color.FromRgb(220, 220, 220));

        private Piece piece;
        private bool enabled;
        private bool selected;

        public BoardField(Position position)
        {
            InitializeComponent();
            this.position = position;
            isEnabled = true;
            Update();            
            MouseEnter += BoardField_MouseEnter;
            MouseLeave += BoardField_MouseLeave;
        }        

        public Position Position
        {
            get
            {
                return this.position;
            }
        }

        public Piece Piece
        {
            get
            {
                return piece;
            }
            set
            {
                piece = value;
                Update();
            }
        }

        public bool isEnabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
                Update();
            }
        }

        public bool isSelected
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;
                Update();
            }
        }

        private bool isDark
        {
            get
            {
                // Nieparzyste
                if ((position.Y + 1) % 2 != 0)
                {
                    return (position.X + 1) % 2 == 0;
                }

                return (position.X + 1) % 2 != 0;
            }
        }

        public void Update()
        {
            UpdateBackground();
            UpdatePiece();
        }

        private Ellipse CreateNormalPiece(PieceColor color)
        {
            Brush backgroud = color == PieceColor.RED ? 
                new SolidColorBrush(Color.FromRgb(188, 0, 2)) : 
                new SolidColorBrush(Color.FromRgb(255, 248, 242));

            Ellipse ellipse = new Ellipse();
            ellipse.Fill = backgroud;
            ellipse.Margin = new Thickness(10);
            return ellipse;            
        }        

        private Ellipse CreateKingPiece(PieceColor color)
        {
            Brush backgroud = color == PieceColor.RED ?
                new SolidColorBrush(Color.FromRgb(188, 0, 2)) :
                new SolidColorBrush(Color.FromRgb(255, 248, 242));

            Ellipse ellipse = new Ellipse();
            ellipse.Fill = backgroud;
            ellipse.Stroke = Brushes.Black;
            ellipse.StrokeThickness = 5;
            ellipse.Margin = new Thickness(10);
            return ellipse;
        }

        private void UpdatePiece()
        {
            if (Piece == null)
            {
                Content = null;
            }
            else if (Piece.Type == PieceType.KING)                
            {
                Content = CreateKingPiece(Piece.Color);                
            }            
            else
            {
                Content = CreateNormalPiece(Piece.Color);
            }            
        }

        private void UpdateBackground()
        {
            UpdateBackground(isSelected);
        }

        private void UpdateBackground(bool isSelected)
        {
            if (isEnabled)
            {
                if (isSelected)
                {
                    if (isDark)
                    {
                        Background = enableSelectedDarkBrush;
                    }
                    else
                    {
                        Background = enableSelectedLightBrush;
                    }
                }
                else
                {
                    if (isDark)
                    {
                        Background = enableDarkBrush;
                    }
                    else
                    {
                        Background = enableLightBrush;
                    }
                }
            } 
            else
            {
                if (isSelected)
                {
                    if (isDark)
                    {
                        Background = disableSelectedDarkBrush;
                    }                    
                }
                else
                {
                    if (isDark)
                    {
                        Background = disableDarkBrush;
                    }
                    else
                    {
                        Background = disableLightBrush;
                    }
                }                
            }
        }        

        private void BoardField_MouseEnter(object sender, MouseEventArgs e)
        {
            //UpdateBackground(true);

            if (isEnabled)
            {
                Cursor = Cursors.Hand;
            } else
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BoardField_MouseLeave(object sender, MouseEventArgs e)
        {
            //UpdateBackground(isSelected);
            Cursor = Cursors.Arrow;
        }
    }
}

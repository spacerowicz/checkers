﻿using Commons;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientApp
{
    /// <summary>
    /// Interaction logic for GamePage.xaml
    /// </summary>
    public partial class GamePage : Page, IServiceCallback
    {
        private GameEngine engine = new GameEngine();
        private BoardField[,] fields = new BoardField[8, 8];
        private Piece selectedPiece = null;
        private List<Position> moveSequence = new List<Position>();

        public GamePage()
        {
            InitializeComponent();            

            for (int i=0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Position position = new Position();
                    position.X = i;
                    position.Y = j;
                    BoardField field = new BoardField(position);
                    this.fields[i, j] = field;
                    Grid.SetColumn(field, i);
                    Grid.SetRow(field, j);
                    this.board.Children.Add(field);

                    field.MouseDown += Field_MouseDown;
                }
            }

            DisableAllFields();
            engine.Initialize();

            this.Loaded += GamePage_Loaded;
        }        

        private void GamePage_Loaded(object sender, RoutedEventArgs e)
        {            
            Ready();
        }

        public void OnGameStateChanged(GameState currentState)
        {
            this.engine.Initialize(currentState);
            Update();

            if (currentState.EndGame)
            {
                if (currentState.Winner == PieceColor.RED)
                {
                    MessageBox.Show("Wygrał gracz posiadający figury czerwone");
                }
                else
                {
                    MessageBox.Show("Wygrał gracz posiadający figury białe");
                }

                if (LoginResult.PlayerColor == PieceColor.WHITE)
                {
                    Ready();
                }
            }
        }

        public LoginResult LoginResult
        {
            get; set;
        }

        public IService Service
        {
            get; set;
        }

        private void doSteps_Click(object sender, RoutedEventArgs e)
        {        
            if (this.selectedPiece == null)
            {
                MessageBox.Show("Nie wybrano figury");
                return;
            }

            if (this.moveSequence.Count == 0)
            {
                MessageBox.Show("Nie wybrano sekwencji ruchu");
                return;
            }

            if (this.engine.MoveSequenceIsAcceptable(selectedPiece.Id, moveSequence) == false)
            {
                MessageBox.Show("Sekwencja nie jest poprawna. Obowiązuje nakaz bicia w przypadku kiedy jest taka możliwość (wykonanie maksymalnej ilości bić nie jest obowiązkowe)");
                return;
            }

            this.doSteps.IsEnabled = false;
            this.resetSteps.IsEnabled = false;

            BackgroundWorker bw = new BackgroundWorker();        
            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            bw.RunWorkerAsync();
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (this.selectedPiece == null)
                {
                    throw new Exception("Selected piece cannot be null");
                }

                if (this.moveSequence.Count == 0)
                {
                    throw new Exception("List of steps cannot be empty");
                }

                Service.Move(LoginResult.SessionKey, selectedPiece.Id, moveSequence);
            } 
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.doSteps.IsEnabled = true;
            this.resetSteps.IsEnabled = true;
            ResetMoveSequence();
        }        

        private void resetSteps_Click(object sender, RoutedEventArgs e)
        {
            ResetMoveSequence();
        }

        private void Update()
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(new Action(delegate {
                    Update();
                }));
                return;
            }

            Piece[,] board = engine.Board;

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    fields[i, j].Piece = board[i, j];
                    fields[i, j].isSelected = selectedPiece != null &&
                        selectedPiece.PositionX == i &&
                        selectedPiece.PositionY == j;                    
                }
            }

            foreach (Position p in moveSequence)
            {
                fields[p.X, p.Y].isSelected = true;
            }

            GameState gameState = engine.GameState;

            DisableAllFields();

            if (gameState.CurrentPlayer == LoginResult.PlayerColor)
            {
                if (selectedPiece == null)
                {
                    foreach (Piece p in engine.ReadyPieces)
                    {
                        fields[p.PositionX, p.PositionY].isEnabled = true;
                    }
                }
                else
                {                                    
                    for (int i=0; i < 8; i++)
                    {
                        for (int j = 0; j < 8; j++)
                        {
                            fields[i, j].isEnabled = engine.CanPieceMove(selectedPiece.Id, moveSequence, i, j);
                        }
                    }
                }
            }            
        }

        private void DisableAllFields()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    fields[i, j].isEnabled = false;
                }
            }
        }

        private void ResetMoveSequence()
        {
            this.selectedPiece = null;
            this.moveSequence.Clear();
            this.Update();
        }

        private void Field_MouseDown(object sender, MouseButtonEventArgs e)
        {
            BoardField field = (BoardField)sender;

            if (field.isEnabled == false)
            {
                return;
            }

            if (this.selectedPiece == null && field.Piece != null)
            {
                field.isSelected = true;
                this.selectedPiece = field.Piece;
                Update();
                return;
            }

            if (this.selectedPiece != null && field.Piece == null)
            {
                moveSequence.Add(field.Position);
                field.isSelected = true;
                Update();
            }
        }        

        void Ready()
        {
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += Ready_DoWork;
            bw.RunWorkerAsync();
        }

        private void Ready_DoWork(object sender, DoWorkEventArgs e)
        {
            Service.PlayerReady(LoginResult.SessionKey);
        }
    }
}

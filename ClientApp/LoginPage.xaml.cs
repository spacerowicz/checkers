﻿using Commons;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientApp
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        public delegate void Login(IService service, LoginResult loginResult);
        public event Login OnLogin;

        private IServiceCallback callback;
        private IService service = null;
        private LoginResult loginResult = null;
        private Exception exception = null;        

        public LoginPage(IServiceCallback callback)
        {
            InitializeComponent();
            this.callback = callback;
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            this.serviceAddress.IsEnabled = false;
            this.loginButton.IsEnabled = false;

            this.service = null;
            this.loginResult = null;
            this.exception = null;

            BackgroundWorker bw = new BackgroundWorker();            
            bw.DoWork += Bw_DoWork;
            bw.RunWorkerCompleted += Bw_RunWorkerCompleted;
            bw.RunWorkerAsync(this.serviceAddress.Text);
        }

        private void Bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (exception != null)
            {
                this.serviceAddress.IsEnabled = true;
                this.loginButton.IsEnabled = true;

                MessageBox.Show(exception.Message, "Wystąpił błąd");
                return;
            }

            this.OnLogin?.Invoke(service, loginResult);
        }

        private void Bw_DoWork(object sender, DoWorkEventArgs e)
        {            
            try
            {
                var binding = new WSDualHttpBinding();
                var endpoint = new EndpointAddress((string)e.Argument);
                var channelFactory = new DuplexChannelFactory<IService>(callback, binding, endpoint);
                               

                service = channelFactory.CreateChannel();
                this.loginResult = service.Login();
                if (loginResult.Status == LoginStatus.NUMBER_OF_PLAYERS_EXCEEDED)
                {
                    throw new Exception("Nie mozna sie zalogowac. Zbyt duzo graczy");
                }
            }
            catch (Exception ex)
            {
                this.exception = ex;
            }
        }
    }
}

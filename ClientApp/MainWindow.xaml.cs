﻿using Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientApp
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IService service;
        private LoginResult loginResult;
        private GamePage gamePage = new GamePage();

        public MainWindow()
        {
            InitializeComponent();
            LoginPage loginPage = new LoginPage(this.gamePage);
            loginPage.OnLogin += LoginPage_OnLogin;
            Content = loginPage;
        }

        private void LoginPage_OnLogin(IService service, LoginResult loginResult)
        {
            this.service = service;            
            this.loginResult = loginResult;
            this.gamePage.Service = service;
            this.gamePage.LoginResult = loginResult;
            this.Content = gamePage;
        }        

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (this.service != null && loginResult != null)
                {
                    service.Logout(loginResult.SessionKey);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

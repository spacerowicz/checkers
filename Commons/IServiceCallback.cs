﻿using System.ServiceModel;

namespace Commons
{
    public interface IServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void OnGameStateChanged(GameState currentState);
    }
}

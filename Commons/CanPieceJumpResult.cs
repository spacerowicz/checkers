﻿namespace Commons
{
    class CanPieceJumpResult
    {
        public CanPieceJumpResult()
        {
            CanJumpLeftTop = false;
            CanJumpRightTop = false;
            CanJumpLeftBottom = false;
            CanJumpRightBottom = false;
        }

        public bool CanJump
        {
            get
            {
                return CanJumpLeftTop || CanJumpRightTop || CanJumpLeftBottom || CanJumpRightBottom;
            }
        }

        public bool CanJumpLeftTop { get; set; }
        public bool CanJumpRightTop { get; set; }
        public bool CanJumpLeftBottom { get; set; }
        public bool CanJumpRightBottom { get; set; }
    }
}
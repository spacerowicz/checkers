﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Commons
{
    [DataContract]
    public class GameState
    {        
        [DataMember]
        public List<Piece> Pieces { get; set; }

        [DataMember]
        public PieceColor CurrentPlayer { get; set; }

        [DataMember]
        public bool EndGame { get; set; }

        [DataMember]
        public PieceColor Winner { get; set; }
    }
}
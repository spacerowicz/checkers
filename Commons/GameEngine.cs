﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Commons
{
    public class GameEngine
    {
        public delegate void StateChanged(GameState state);
        public event StateChanged OnStateChanged;

        private Dictionary<int, Piece> pieces = new Dictionary<int, Piece>();
        private PieceColor currentPlayer;
        private bool endGame = false;
        private PieceColor winner;
        private const int boardSize = 8;

        public void Initialize()
        {
            int id = 0;
            pieces.Clear();

            // White pieces
            for (int i = 1; i < 8; i += 2)
            {
                Piece piece = new Piece()
                {
                    Id = id,
                    Color = PieceColor.WHITE,
                    PositionX = i,
                    PositionY = 0,
                    Type = PieceType.MEN
                };

                this.pieces.Add(piece.Id, piece);
                id++;
            }

            for (int i = 0; i < 8; i += 2)
            {
                Piece piece = new Piece()
                {
                    Id = id,
                    Color = PieceColor.WHITE,
                    PositionX = i,
                    PositionY = 1,
                    Type = PieceType.MEN
                };

                this.pieces.Add(piece.Id, piece);
                id++;
            }

            for (int i = 1; i < 8; i += 2)
            {
                Piece piece = new Piece()
                {
                    Id = id,
                    Color = PieceColor.WHITE,
                    PositionX = i,
                    PositionY = 2,
                    Type = PieceType.MEN
                };

                this.pieces.Add(piece.Id, piece);
                id++;
            }

            // Red pieces
            for (int i = 0; i < 8; i += 2)
            {
                Piece piece = new Piece()
                {
                    Id = id,
                    Color = PieceColor.RED,
                    PositionX = i,
                    PositionY = 5,
                    Type = PieceType.MEN
                };

                this.pieces.Add(piece.Id, piece);
                id++;
            }

            for (int i = 1; i < 8; i += 2)
            {
                Piece piece = new Piece()
                {
                    Id = id,
                    Color = PieceColor.RED,
                    PositionX = i,
                    PositionY = 6,
                    Type = PieceType.MEN
                };

                this.pieces.Add(piece.Id, piece);
                id++;
            }

            for (int i = 0; i < 8; i += 2)
            {
                Piece piece = new Piece()
                {
                    Id = id,
                    Color = PieceColor.RED,
                    PositionX = i,
                    PositionY = 7,
                    Type = PieceType.MEN
                };

                this.pieces.Add(piece.Id, piece);
                id++;
            }

            this.currentPlayer = PieceColor.RED;
            this.endGame = false;
            this.winner = PieceColor.RED;
            this.OnStateChanged?.Invoke(this.GameState);
        }

        public void Initialize(GameState state)
        {
            this.currentPlayer = state.CurrentPlayer;
            this.endGame = state.EndGame;
            this.pieces = this.CreatePieceDictionary(state.Pieces);
            this.winner = state.Winner;
        }

        public Piece[,] Board
        {
            get
            {
                List<Piece> pieces = new List<Piece>();

                foreach (Piece p in this.pieces.Values)
                {
                    pieces.Add(Piece.FromPiece(p));
                }

                return CreateBoard(pieces);
            }
        }

        public bool CanPieceMove(int pieceId, List<Position> currentMoves, int newMovePositionX, int newMovePositionY)
        {
            if (!this.pieces.ContainsKey(pieceId))
            {
                throw new ArgumentException("Piece not found");
            }

            Piece piece = this.pieces[pieceId];
            return this.CanPieceMove(piece, currentMoves, newMovePositionX, newMovePositionY);
        }

        public bool MoveSequenceIsAcceptable(int pieceId, List<Position> moveSquence)
        {
            if (!this.pieces.ContainsKey(pieceId))
            {
                throw new ArgumentException("Piece not found");
            }

            Piece piece = this.pieces[pieceId];
            return this.MoveSequenceIsAcceptable(piece, moveSquence);
        }

        public List<Piece> FindPlayerPieces(PieceColor playerColor)
        {
            List<Piece> playerPieces = this.pieces.Values
                    .Where(i => i.Color == playerColor)
                    .ToList<Piece>();

            return playerPieces;
        }

        public List<Piece> ReadyPieces
        {
            get
            {
                List<Piece> playerPieces = this.pieces.Values
                    .Where(i => i.Color == currentPlayer)
                    .ToList<Piece>();

                if (playerPieces.Count == 0)
                {
                    return new List<Piece>();
                }

                Piece[,] board = this.CreateBoard(this.pieces.Values.ToList());

                List<Piece> canJumpPieces = playerPieces
                    .Where(i => this.CanPieceJump((Piece)i, board).CanJump == true)
                    .ToList();

                List<Piece> canMovePieces = playerPieces
                    .Where(i => this.CanPieceMove((Piece)i, board) == true)
                    .ToList();

                List <Piece> resut = canJumpPieces.Count > 0 ? canJumpPieces : canMovePieces; 
                
                for (int i=0; i < resut.Count; i++)
                {
                    resut[i] = Piece.FromPiece(resut[i]);
                }

                return resut;
            }
        }

        public GameState GameState
        {
            get
            {
                List<Piece> statePieces = new List<Piece>();

                foreach (Piece p in this.pieces.Values)
                {
                    statePieces.Add(Piece.FromPiece(p));
                }

                return new GameState
                {
                    Pieces = statePieces,
                    CurrentPlayer = this.currentPlayer,
                    EndGame = this.endGame,
                    Winner = this.winner
                };
            }
        }

        public void Move(int pieceId, List<Position> moveSquence)
        {   
            if (!this.pieces.ContainsKey(pieceId))
            {
                throw new ArgumentException(string.Format("Piece #{0} not found", pieceId));
            }
            
            if (ReadyPieces.Where(i => i.Id == pieceId).ToList().Count == 0)
            {
                throw new ArgumentException("Piece is not ready to move");
            }

            if (MoveSequenceIsAcceptable(pieceId, moveSquence) == false)
            {
                throw new ArgumentException("Move sequence is not completed");
            }

            Piece piece = this.pieces[pieceId];
            Piece[,] board = this.CreateBoard(this.pieces.Values.ToList());
            MovePieceSequentialResult result = this.MovePieceSequential(piece, board, moveSquence);
            
            foreach (Piece p in result.DeletedPieces)
            {
                this.pieces.Remove(p.Id);
            }

            this.pieces[result.Piece.Id] = Piece.FromPiece(result.Piece);
            this.winner = this.currentPlayer;

            if (this.currentPlayer == PieceColor.RED)
            {
                this.currentPlayer = PieceColor.WHITE;
            }
            else
            {
                this.currentPlayer = PieceColor.RED;
            }

            PromoteToKings();

            this.endGame = GameOver;        
            this.OnStateChanged?.Invoke(this.GameState);
        }

        private bool GameOver
        {
            get
            {
                return this.ReadyPieces.Count == 0;
            }
        }

        private bool MoveSequenceIsAcceptable(Piece piece, List<Position> moveSquence)
        {
            if (moveSquence == null)
            {
                throw new ArgumentNullException("Move squence cannot be null");
            }

            if (moveSquence.Count == 0)
            {
                return false;
            }

            Piece[,] board = this.CreateBoard(new List<Piece>(this.pieces.Values));            

            MovePieceSequentialResult moveResult = this.MovePieceSequential(
                piece, board, moveSquence);

            if (IsNormalMove(piece, moveSquence[0].X, moveSquence[0].Y))
            {
                if (moveSquence.Count > 1)
                {
                    throw new Exception("To many moves");
                }

                return true;
            }

            return !this.CanPieceJump(moveResult.Piece, moveResult.Board).CanJump;
        }

        private bool CanPieceMove(Piece piece, List<Position> currentMoves, int newMovePositionX, int newMovePositionY)
        {
            if (currentMoves == null)
            {
                throw new ArgumentNullException("List of current moves cannot be null");
            }            
        
            Piece[,] board = this.CreateBoard(new List<Piece>(this.pieces.Values));

            if (currentMoves.Count == 0)
            {
                return this.CanPieceMove(piece, board, newMovePositionX, newMovePositionY);
            }

            MovePieceSequentialResult movePieceSequentialResult = this.MovePieceSequential(
                piece, board, currentMoves);
           
            bool canMove = this.CanPieceMove(
                movePieceSequentialResult.Piece, 
                movePieceSequentialResult.Board, 
                newMovePositionX, 
                newMovePositionY);

            if (currentMoves.Count > 1 && IsNormalMove(piece, currentMoves[0].X, currentMoves[0].Y))
            {
                throw new Exception("To many moves");
            }

            if (currentMoves.Count > 0)
            {               
                return canMove &&
                    IsJump(piece, currentMoves[0].X, currentMoves[0].Y) &&
                    IsJump(movePieceSequentialResult.Piece, newMovePositionX, newMovePositionY);
            }

            return canMove;
        }        

        private Dictionary<int, Piece> CreatePieceDictionary(List<Piece> pieces)
        {
            if (pieces == null)
            {
                throw new ArgumentNullException("List of pieces cannot be null");
            }

            Dictionary<int, Piece> dictionary = new Dictionary<int, Piece>();

            foreach (Piece p in pieces)
            {
                if (p == null)
                {
                    throw new ArgumentNullException("Piece cannot be null");
                }

                dictionary.Add(p.Id, Piece.FromPiece(p));
            }

            return dictionary;
        }

        private Piece[,] CreateBoard(List<Piece> pieces)
        {
            if (pieces == null)
            {
                throw new ArgumentNullException("List of pieces cannot be null");
            }

            Piece[,] board = new Piece[boardSize, boardSize];

            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    board[i, j] = null;
                }
            }

            foreach (Piece p in pieces)
            {
                board[p.PositionX, p.PositionY] = p;
            };

            return board;
        }

        private bool CanPieceMove(Piece piece, Piece[,] board)
        {            
            if (piece.PositionX - 2 >= 0 && 
                piece.PositionY - 2 >= 0 && 
                this.CanPieceMove(piece, board, piece.PositionX - 2, piece.PositionY - 2))
            {
                return true;
            }

            if (piece.PositionX + 2 < boardSize &&
                piece.PositionY - 2 >= 0 &&
                this.CanPieceMove(piece, board, piece.PositionX + 2, piece.PositionY - 2))
            {
                return true;
            }

            if (piece.PositionX - 2 >= 0 &&
                piece.PositionY + 2 < boardSize &&
                this.CanPieceMove(piece, board, piece.PositionX - 2, piece.PositionY + 2))
            {
                return true;
            }

            if (piece.PositionX + 2 < boardSize &&
                piece.PositionY + 2 < boardSize &&
                this.CanPieceMove(piece, board, piece.PositionX + 2, piece.PositionY + 2))
            {
                return true;
            }            

            if (piece.PositionX - 1 >= 0 &&
                piece.PositionY - 1 >= 0 &&
                this.CanPieceMove(piece, board, piece.PositionX - 1, piece.PositionY - 1))
            {
                return true;
            }

            if (piece.PositionX + 1 < boardSize &&
                piece.PositionY - 1 >= 0 &&
                this.CanPieceMove(piece, board, piece.PositionX + 1, piece.PositionY - 1))
            {
                return true;
            }

            if (piece.PositionX - 1 >= 0 &&
                piece.PositionY + 1 < boardSize &&
                this.CanPieceMove(piece, board, piece.PositionX - 1, piece.PositionY + 1))
            {
                return true;
            }

            if (piece.PositionX + 1 < boardSize &&
                piece.PositionY + 1 < boardSize &&
                this.CanPieceMove(piece, board, piece.PositionX + 1, piece.PositionY + 1))
            {
                return true;
            }

            return false;
        }

        private bool CanPieceMove(Piece piece, Piece[,] board, int newPositionX, int newPositionY)
        {
            ValidatePieceAndBoard(piece, board);
            ValidatePositions(newPositionX, newPositionY);

            CanPieceJumpResult jump = this.CanPieceJump(piece, board);

            if (jump.CanJump)
            {
                if (jump.CanJumpLeftTop && 
                    piece.PositionX - 2 == newPositionX && 
                    piece.PositionY - 2 == newPositionY)
                {
                    return true;
                }

                if (jump.CanJumpRightTop &&
                    piece.PositionX + 2 == newPositionX &&
                    piece.PositionY - 2 == newPositionY)
                {
                    return true;
                }

                if (jump.CanJumpLeftBottom &&
                    piece.PositionX - 2 == newPositionX &&
                    piece.PositionY + 2 == newPositionY)
                {
                    return true;
                }

                if (jump.CanJumpRightBottom &&
                    piece.PositionX + 2 == newPositionX &&
                    piece.PositionY + 2 == newPositionY)
                {
                    return true;
                }

                return false;
            }

            if (Math.Abs(piece.PositionX - newPositionX) != 1 || 
                Math.Abs(piece.PositionY - newPositionY) != 1)
            {
                return false;
            }

            if (piece.Type == PieceType.KING)
            {
                return board[newPositionX, newPositionY] == null;
            }
            else if (piece.Color == PieceColor.RED)
            {
                return board[newPositionX, newPositionY] == null && newPositionY < piece.PositionY;
            }

            return board[newPositionX, newPositionY] == null && newPositionY > piece.PositionY;
        }

        private MovePieceSequentialResult MovePieceSequential(
            Piece piece, Piece[,] board, List<Position> moves)
        {
            if (moves == null)
            {
                throw new ArgumentNullException("List of moves cannot be null");
            }

            if (moves.Count == 0)
            {
                throw new ArgumentException("List of moves cannot be empty");
            }

            Piece resultPiece = piece;
            List<Piece> resultDeletedPieces = new List<Piece>();
            Piece[,] resultBoard = board;
            
            foreach (Position m in moves) 
            {
                if (m == null)
                {
                    throw new ArgumentNullException("Move cannot be null");
                }

                int newPositionX = m.X;
                int newPositionY = m.Y;

                MovePieceResult movePieceResult = this.MovePiece(resultPiece, resultBoard, newPositionX, newPositionY);
                resultPiece = Piece.FromPiece(movePieceResult.Piece);
                resultBoard = movePieceResult.Board;

                if (movePieceResult.DeletedPiece != null)
                {
                    resultDeletedPieces.Add(movePieceResult.DeletedPiece);
                }                
            }

            return new MovePieceSequentialResult(resultPiece, resultDeletedPieces, resultBoard);
        }

        private MovePieceResult MovePiece(Piece piece, Piece[,] board, int newPositionX, int newPositionY)
        {
            ValidatePieceAndBoard(piece, board);
            ValidatePositions(newPositionX, newPositionY);

            if (!this.CanPieceMove(piece, board, newPositionX, newPositionY))
            {
                throw new ArgumentException(string.Format("Cannot move piece from position ({0}, {1}) to ({2}, {3})",
                    piece.PositionX, piece.PositionY, newPositionX, newPositionY));
            }
            
            Piece resultPiece = Piece.FromPiece(piece);
            Piece[,] resultBoard = new Piece[board.GetLength(0), board.GetLength(1)];
            Array.Copy(board, resultBoard, board.Length);
            
            if (this.IsJump(resultPiece, newPositionX, newPositionY))
            {
                int deltaX = newPositionX - resultPiece.PositionX;
                int deltaY = newPositionY - resultPiece.PositionY;

                int deletedPieceX = resultPiece.PositionX + (deltaX / 2);
                int deletedPieceY = resultPiece.PositionY + (deltaY / 2);

                Piece deletedPiece = resultBoard[deletedPieceX, deletedPieceY];

                resultBoard[deletedPieceX, deletedPieceY] = null;
                resultBoard[resultPiece.PositionX, resultPiece.PositionY] = null;                
                resultBoard[newPositionX, newPositionY] = resultPiece;

                resultPiece.PositionX = newPositionX;
                resultPiece.PositionY = newPositionY;

                return new MovePieceResult(resultPiece, deletedPiece, resultBoard);
            }

            if (this.IsNormalMove(resultPiece, newPositionX, newPositionY))
            {            
                resultBoard[resultPiece.PositionX, resultPiece.PositionY] = null;
                resultBoard[newPositionX, newPositionY] = resultPiece;

                resultPiece.PositionX = newPositionX;
                resultPiece.PositionY = newPositionY;

                return new MovePieceResult(resultPiece, null, resultBoard);
            }

            throw new Exception("Unsupported move type");
        }

        private CanPieceJumpResult CanPieceJump(Piece piece, Piece[,] board)
        {
            this.ValidatePieceAndBoard(piece, board);

            bool canJumpLeftTop = piece.PositionX - 2 >= 0 && piece.PositionY - 2 >= 0 &&
                    board[piece.PositionX - 1, piece.PositionY - 1] != null &&
                    board[piece.PositionX - 1, piece.PositionY - 1].Color != piece.Color &&
                    board[piece.PositionX - 2, piece.PositionY - 2] == null;

            bool canJumpRightTop = piece.PositionX + 2 < boardSize && piece.PositionY - 2 >= 0 &&
                board[piece.PositionX + 1, piece.PositionY - 1] != null &&
                board[piece.PositionX + 1, piece.PositionY - 1].Color != piece.Color &&
                board[piece.PositionX + 2, piece.PositionY - 2] == null;

            bool canJumpLeftBottom = piece.PositionX - 2 >= 0 && piece.PositionY + 2 < boardSize &&
                board[piece.PositionX - 1, piece.PositionY + 1] != null &&
                board[piece.PositionX - 1, piece.PositionY + 1].Color != piece.Color &&
                board[piece.PositionX - 2, piece.PositionY + 2] == null;

            bool canJumpRightBottom = piece.PositionX + 2 < boardSize && piece.PositionY + 2 < boardSize &&
                board[piece.PositionX + 1, piece.PositionY + 1] != null &&
                board[piece.PositionX + 1, piece.PositionY + 1].Color != piece.Color &&
                board[piece.PositionX + 2, piece.PositionY + 2] == null;

            CanPieceJumpResult result = new CanPieceJumpResult();

            if (piece.Type == PieceType.KING)
            {
                result.CanJumpLeftTop = canJumpLeftTop;
                result.CanJumpRightTop = canJumpRightTop;            
                result.CanJumpLeftBottom = canJumpLeftBottom;
                result.CanJumpRightBottom = canJumpRightBottom;
                return result;
            }
            else if (piece.Color == PieceColor.RED)
            {
                result.CanJumpLeftTop = canJumpLeftTop;
                result.CanJumpRightTop = canJumpRightTop;                
                return result;                
            }
            
            result.CanJumpLeftBottom = canJumpLeftBottom;
            result.CanJumpRightBottom = canJumpRightBottom;
            return result;
        }

        private void ValidatePieceAndBoard(Piece piece, Piece[,] board)
        {
            if (piece == null)
            {
                throw new ArgumentNullException("Piece cannot be null");
            }

            if (piece.PositionX < 0 || piece.PositionX > boardSize - 1 ||
                piece.PositionY < 0 || piece.PositionY > boardSize - 1)
            {
                throw new ArgumentException("Incorrect piece position");
            }

            if (board == null)
            {
                throw new ArgumentNullException("Board cannot be null");
            }

            if (board.GetLength(0) != boardSize || board.GetLength(1) != boardSize)
            {
                throw new ArgumentException("Incorrect size of board");
            }

            if (board[piece.PositionX, piece.PositionY] == null || 
                board[piece.PositionX, piece.PositionY].Id != piece.Id)
            {
                throw new ArgumentException("The piece is not a part of board or the board is not correct");
            }
        }

        private void ValidatePositions(int positionX, int positionY)
        {
            this.ValidatePosition(positionX);
            this.ValidatePosition(positionY);
        }

        private void ValidatePosition(int position)
        {                
            if (position < 0 || position > boardSize - 1)
            {
                throw new ArgumentException("Incorrect position");
            }            
        }

        private bool IsJump(int positionX, int positionY, int newPositionX, int newPositionY)
        {
            ValidatePositions(positionX, positionY);            
            ValidatePositions(newPositionX, newPositionY);

            return Math.Abs(positionX - newPositionX) == 2 &&
                Math.Abs(positionY - newPositionY) == 2;
        }

        private bool IsJump(Piece piece, int newPositionX, int newPositionY)
        {
            if (piece == null)
            {
                throw new ArgumentNullException("Piece cannot be null");
            }
            return this.IsJump(piece.PositionX, piece.PositionY, newPositionX, newPositionY);
        }

        private bool IsNormalMove(int positionX, int positionY, int newPositionX, int newPositionY)
        {
            ValidatePositions(positionX, positionY);
            ValidatePositions(newPositionX, newPositionY);

            return Math.Abs(positionX - newPositionX) == 1 &&
                Math.Abs(positionY - newPositionY) == 1;
        }

        private bool IsNormalMove(Piece piece, int newPositionX, int newPositionY)
        {
            if (piece == null)
            {
                throw new ArgumentNullException("Piece cannot be null");
            }
            return this.IsNormalMove(piece.PositionX, piece.PositionY, newPositionX, newPositionY);
        }

        private void PromoteToKings()
        {
            foreach (Piece p in this.pieces.Values)
            {
                if (p == null)
                {
                    throw new Exception("Piece cannot be null");
                }

                if (p.Color == PieceColor.RED && p.PositionY == 0)
                {
                    p.Type = PieceType.KING;
                }

                if (p.Color == PieceColor.WHITE && p.PositionY == boardSize - 1)
                {
                    p.Type = PieceType.KING;
                }
            }
        }
    }    
}

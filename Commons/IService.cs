﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace Commons
{
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IServiceCallback))]
    public interface IService
    {
        [OperationContract]
        LoginResult Login();

        [OperationContract]
        bool CanPieceMove(string sessionKey, int pieceId, List<Position> currentMoves, int newMovePositionX, int newMovePositionY);        

        [OperationContract]
        GameState GameState(string sessionKey);

        [OperationContract]
        void Move(string sessionKey, int pieceId, List<Position> moveSquence);

        [OperationContract]
        void PlayerReady(string sessionKey);

        [OperationContract]
        void Logout(string sessionKey);
    }
}
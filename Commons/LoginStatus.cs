﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commons
{
    public enum LoginStatus
    {
        OK = 0,
        NUMBER_OF_PLAYERS_EXCEEDED = 1
    }
}
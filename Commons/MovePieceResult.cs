﻿namespace Commons
{
    class MovePieceResult
    {
        private Piece piece;
        private Piece deletedPiece;
        private Piece[,] board;

        public MovePieceResult(Piece piece, Piece deletedPiece, Piece[,] board)
        {
            this.piece = piece;
            this.deletedPiece = deletedPiece;
            this.board = board;
        }

        public Piece Piece
        {
            get
            {
                return this.piece;
            }
        }

        public Piece DeletedPiece
        {
            get
            {
                return this.deletedPiece;
            }
        }

        public Piece[,] Board
        {
            get
            {
                return this.board;
            }
        }
    }
}
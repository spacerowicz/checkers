﻿using Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Commons
{
    public class LoginResult
    {
        public string SessionKey { get; set; }    
        public PieceColor PlayerColor { get; set; }
        public LoginStatus Status { get; set; }
    }
}
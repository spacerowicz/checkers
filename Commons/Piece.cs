﻿using System;

namespace Commons
{
    public class Piece
    {
        public static Piece FromPiece(Piece piece)
        {
            if (piece == null)
            {
                throw new ArgumentNullException("Piece cannot be null");
            }

            Piece result = new Piece();
            result.Id = piece.Id;
            result.PositionX = piece.PositionX;
            result.PositionY = piece.PositionY;
            result.Color = piece.Color;
            result.Type = piece.Type;
            return result;
        }

        public int Id { get; set; }
        public int PositionX { get; set; }
        public int PositionY { get; set; }
        public PieceColor Color { get; set; }
        public PieceType Type { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace Commons
{
    class MovePieceSequentialResult
    {
        private Piece piece;
        private List<Piece> deletedPieces;
        private Piece[,] board;

        public MovePieceSequentialResult(Piece piece, List<Piece> deletedPieces, Piece[,] board)
        {
            this.piece = piece;
            this.deletedPieces = deletedPieces;
            this.board = board;
        }

        public Piece Piece
        {
            get
            {
                return this.piece;
            }
        }

        public List<Piece> DeletedPieces
        {
            get
            {
                return this.deletedPieces;
            }
        }

        public Piece[,] Board
        {
            get
            {
                return this.board;
            }
        }
    }
}
﻿using Commons;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ServerApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ServiceHost serviceHost;
        private CheckersService service;

        public MainWindow()
        {
            InitializeComponent();
            this.serviceAddress.IsEnabled = true;
            this.StartServer.IsEnabled = true;
            this.StopServer.IsEnabled = false;
        }        

        private void StartServer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                service = new CheckersService();
                service.OnLoginRedPlayer += Service_OnLoginRedPlayer; ;
                service.OnLoginWhitePlayer += Service_OnLoginWhitePlayer; ;
                service.OnLogoutRedPlayer += Service_OnLogoutRedPlayer; ;
                service.OnLogoutWhitePlayer += Service_OnLogoutWhitePlayer; ;
                service.OnPlayerRejected += Service_OnPlayerRejected; ;
                service.OnPlayerMoved += Service_OnPlayerMoved; ;
                service.OnBeginGame += Service_OnBeginGame; ;
                service.OnEndGame += Service_OnEndGame; ;

                serviceHost = new ServiceHost(service);
                serviceHost.AddServiceEndpoint(
                    typeof(IService), 
                    new WSDualHttpBinding(), 
                    new Uri(serviceAddress.Text));
                
                serviceHost.Opened += ServiceHost_Opened;
                serviceHost.Closed += ServiceHost_Closed;
                serviceHost.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Service_OnLoginRedPlayer()
        {
            Log("Gracz posługujący się czerwonymi figurami został zalogowany");
        }

        private void Service_OnLoginWhitePlayer()
        {
            Log("Gracz posługujący się białymi figurami został zalogowany");
        }

        private void Service_OnLogoutRedPlayer()
        {
            Log("Gracz posługujący się czerwonymi figurami został wylogowany");
        }

        private void Service_OnLogoutWhitePlayer()
        {
            Log("Gracz posługujący się białymi figurami został wylogowany");
        }

        private void Service_OnPlayerRejected()
        {
            Log("Odrzucono gracza który próbował się zalogować (zbyt wielu graczy)");
        }

        private void Service_OnPlayerMoved(PieceColor player)
        {
            if (player == PieceColor.RED)
            {
                Log("Gracz posługujący się czerwonymi figurami wykonał ruch");
            }                
            else
            {
                Log("Gracz posługujący się białymi figurami wykonał ruch");
            }
        }

        private void Service_OnBeginGame()
        {
            Log("Rozpoczęto nową rozgrywkę");
        }

        private void Service_OnEndGame(PieceColor winner)
        {
            if (winner == PieceColor.RED)
            {
                Log("Wygrał gracz posługujący się czerwonymi figurami");
            }
            else
            {
                Log("Wygrał gracz posługujący się białymi figurami");
            }
        }

        private void ServiceHost_Opened(object sender, EventArgs e)
        {            
            this.serviceAddress.IsEnabled = false;
            this.StartServer.IsEnabled = false;
            this.StopServer.IsEnabled = true;
            Log("Uruchomiono serwer");
        }

        private void ServiceHost_Closed(object sender, EventArgs e)
        {
            this.serviceAddress.IsEnabled = true;
            this.StartServer.IsEnabled = true;
            this.StopServer.IsEnabled = false;
            Log("Zatrzymano serwer");
        }

        private void StopServer_Click(object sender, RoutedEventArgs e)
        {
            try
            {                
                serviceHost.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {                
                if (this.serviceHost != null)
                {
                    this.serviceHost.Close();
                    this.serviceHost = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }

        private void Log(string message)
        {
            if (!Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(new Action(delegate {
                }));
                return;
            }

            logs.Text += message + "\r\n";
            this.logs.Focus();
            this.logs.CaretIndex = this.logs.Text.Length;
            this.logs.ScrollToEnd();
        }
    }
}

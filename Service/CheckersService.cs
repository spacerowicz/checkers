﻿using Commons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class CheckersService : IService
    {
        public delegate void LoginRedPlayer();
        public delegate void LoginWhitePlayer();
        public delegate void LogoutRedPlayer();
        public delegate void LogoutWhitePlayer();
        public delegate void PlayerRejected();
        public delegate void PlayerMoved(PieceColor player);
        public delegate void BeginGame();
        public delegate void EndGame(PieceColor winner);

        public event LoginRedPlayer OnLoginRedPlayer;
        public event LoginWhitePlayer OnLoginWhitePlayer;
        public event LogoutRedPlayer OnLogoutRedPlayer;
        public event LogoutWhitePlayer OnLogoutWhitePlayer;
        public event PlayerRejected OnPlayerRejected;
        public event PlayerMoved OnPlayerMoved;
        public event BeginGame OnBeginGame;
        public event EndGame OnEndGame;

        private GameEngine engine = new GameEngine();
        private string firstPlayerSessionKey = null;
        private string secondPlayerSessionKey = null;
        private bool firstPlayerReady = false;
        private bool secondPlayerReady = false;
        private IServiceCallback firstPlayerCallback;
        private IServiceCallback secondPlayerCallback;

        public CheckersService()
        {
            engine.OnStateChanged += Engine_OnStateChanged;
        }

        public LoginResult Login()
        {
            LoginResult result = new LoginResult();
            result.SessionKey = Guid.NewGuid().ToString();

            if (this.firstPlayerSessionKey == null)
            {
                this.firstPlayerSessionKey = result.SessionKey;
                this.firstPlayerCallback = Callback;
                result.PlayerColor = PieceColor.RED;
                result.Status = LoginStatus.OK;
                OnLoginRedPlayer?.Invoke();
            }
            else if (this.secondPlayerSessionKey == null)
            {
                this.secondPlayerSessionKey = result.SessionKey;
                this.secondPlayerCallback = Callback;
                result.PlayerColor = PieceColor.WHITE;
                result.Status = LoginStatus.OK;
                OnLoginWhitePlayer?.Invoke();
            }
            else
            {
                result.Status = LoginStatus.NUMBER_OF_PLAYERS_EXCEEDED;
                OnPlayerRejected?.Invoke();
            }

            return result;
        }

        public bool CanPieceMove(string sessionKey, int pieceId, List<Position> currentMoves, int newMovePositionX, int newMovePositionY)
        {
            CheckSessionKey(sessionKey);
            return this.engine.CanPieceMove(pieceId, currentMoves,
                newMovePositionX, newMovePositionY);
        }

        public GameState GameState(string sessionKey)
        {
            CheckSessionKey(sessionKey);
            return this.engine.GameState;
        }

        public void Logout(string sessionKey)
        {
            CheckSessionKey(sessionKey);

            if (sessionKey == firstPlayerSessionKey)
            {
                firstPlayerSessionKey = null;
                firstPlayerCallback = null;
                firstPlayerReady = false;
                OnLogoutRedPlayer?.Invoke();
                return;
            }

            secondPlayerSessionKey = null;
            secondPlayerCallback = null;
            secondPlayerReady = false;
            OnLogoutWhitePlayer?.Invoke();
        }

        public void Move(string sessionKey, int pieceId, List<Position> moveSquence)
        {
            PieceColor playerColor = GetPlayerColor(sessionKey);
            GameState gameState = engine.GameState;

            if (gameState.CurrentPlayer != playerColor)
            {
                throw new Exception("Incorrect player");
            }

            bool foundPiece = engine.FindPlayerPieces(playerColor)
                .Where(i => i.Id == pieceId)
                .ToList()
                .Count > 0;

            if (foundPiece == false)
            {
                throw new Exception("Piece not found");
            }

            this.engine.Move(pieceId, moveSquence);
            OnPlayerMoved?.Invoke(playerColor);
        }

        private void Engine_OnStateChanged(GameState state)
        {
            if (state.EndGame)
            {
                OnEndGame?.Invoke(state.Winner);
            }

            if (firstPlayerCallback != null)
            {
                firstPlayerCallback.OnGameStateChanged(state);
            }

            if (secondPlayerCallback != null)
            {
                secondPlayerCallback.OnGameStateChanged(state);
            }
        }

        private void CheckSessionKey(string sessionKey)
        {
            if (sessionKey != firstPlayerSessionKey &&
                sessionKey != secondPlayerSessionKey)
            {
                throw new ArgumentException("Incorrect session key");
            }
        }

        private PieceColor GetPlayerColor(string sessionKey)
        {
            CheckSessionKey(sessionKey);
            if (sessionKey == firstPlayerSessionKey)
            {
                return PieceColor.RED;
            }
            return PieceColor.WHITE;
        }

        public void PlayerReady(string sessionKey)
        {
            CheckSessionKey(sessionKey);

            if (sessionKey == firstPlayerSessionKey)
            {
                this.firstPlayerReady = true;
            }

            if (sessionKey == secondPlayerSessionKey)
            {
                this.secondPlayerReady = true;
            }
            
            if (firstPlayerReady && secondPlayerReady)
            {
                OnBeginGame?.Invoke();
                engine.Initialize();
            }
        }

        private IServiceCallback Callback
        {
            get
            {
                return OperationContext.Current.GetCallbackChannel<IServiceCallback>();
            }
        }
    }
}
